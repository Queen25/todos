<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
class TodosController extends Controller
{
	protected $todos;

	public function __construct(Todo $todos)
	{	
		$this->todos = $todos;
	}

    public function index()
    {
    	return $this->todos->all();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'priority' => 'required'
        ]);
        
    	$data = $this->todos->create($request->all());

    	return response()->json($data,200);
    }

    public function show($id)
    {
    	$data = $this->todos->findorFail($id);

    	return response()->json(['data' => $data,'status' => 200],200);
    }

    public function update(Request $request, $id)
    {
    	$data = $this->todos->findorFail($id);

    	$data->name = $request->name;
    	$data->priority = $request->priority;
    	$data->location = $request->location;
    	$data->time_start = $request->time_start;
    	$data->username = $request->username;
    	$data->password = $request->password;
    	$data->save();

    	return response()->json(['message' => 'Success Update' ,'status' => 201],201);
    }

    public function destroy($id)
    {
    	$data = $this->todos->findorFail($id);

    	$data->delete();

    	return response()->json(['message' => 'Success Delete' ,'status' => 204],204);
    }

    public function getOrderBy($order)
    {
        $data = $this->todos->orderBy('priority',$order)->get();

        return response()->json(['data' => $data,'success' => 200],200);
    }


    public function getTimeOrder($time)
    {
        $data = $this->todos->where('time_start','<=',$time)->get();

        return response()->json(['data' => $data,'success' => 200],200);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
    	'name',
    	'priority',
    	'location',
    	'time_start',
    	'username',
    	'password'
    ];
}

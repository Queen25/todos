<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){
	Route::get('todos','TodosController@index');
	Route::get('todos/{id}','TodosController@show');
	Route::put('todos/{id}','TodosController@update');
	Route::post('todos','TodosController@store');
	Route::delete('todos/{id}','TodosController@destroy');
	Route::get('todos/order/{order}','TodosController@getOrderBy');
	Route::get('todos/order_time/{order}','TodosController@getTimeOrder');
});